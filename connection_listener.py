import multiprocessing
import multiprocessing.reduction
import socket
# For Python 3
try:
  import Queue
except ImportError:
  import queue as Queue

import config_manager
import aws_exceptions

# Config file path
CONFIG_PATH = 'config/server.cfg'

# Settings keys
PORT = 'port'
HOST = 'host'

# Default settings
DEFAULT_PORT = 5001
DEFAULT_HOST = 'localhost'

class ConnectionListener(config_manager.Configurable, multiprocessing.Process):
    """A custom process that listens for connections to the server."""
    
    def __init__(self, queue):
        """Initialise the process."""
        multiprocessing.Process.__init__(self)
        config_manager.Configurable.__init__(self, CONFIG_PATH)
        self.host = DEFAULT_HOST
        self.port = DEFAULT_PORT
        self.load_config()
        self.set_config()
        self.connections_queue = queue
        self.sock = socket.socket()
        try:
            self.sock.bind((self.host, self.port))
        except socket.error:
            error_message = "Could not bind to port no. " + str(self.port)
            raise aws_exceptions.BindError(error_message)
    
    def set_config(self):
        """Process the main server config file and load its settings."""
        if HOST in self.settings.keys():
            self.host = self.settings[HOST]
        else:
            print("No '", HOST, "' found in '", config_path, "'")
            print("Using default host: ", DEFAULT_HOST)
        if PORT in self.settings.keys():
            try:
                self.port = int(self.settings[PORT])
            except ValueError:
                error_message = "'" + self.settings[PORT] + "' is not an integer"
                raise aws_exceptions.FatalError(error_message)
        else:
            print("No '", PORT, "' found in '", config_path, "'")
            print("Using default port: ", DEFAULT_PORT)
    
    def run(self):
        """Run the connection listener process."""
        try:
            # Specifies maximum number of queued connections
            self.sock.listen(5)
            while True:
                connection, addr = self.sock.accept()
                connection.setblocking(False)
                rebuild_func, unbuilt_connection = multiprocessing.reduction.reduce_socket(connection)
                try:
                    self.connections_queue.put(unbuilt_connection, False)
                except Queue.Full:
                    pass
        except KeyboardInterrupt:
            self.stop()
    
    def stop(self):
        """Cleanup the process so the program can exit cleanly."""
        self.connections_queue.close()
        self.sock.shutdown(socket.SHUT_RDWR)
        self.sock.close()
