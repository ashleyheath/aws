import os.path

import aws_exceptions
import config_manager

PATH_SYMBOL = '*'

CONFIG_PATH = 'config/paths.cfg'

class PathMapper(config_manager.Configurable):
    """Manages the mappings of web address to local file."""
        
    def __init__(self):
        """Initialise the path mapper."""
        config_manager.Configurable.__init__(self, CONFIG_PATH)
        self.patterns = {}
        self.mappings = {}
        self.load_config()
        self.set_config()
    
    def set_config(self):
        """Process the paths config file and load its settings."""
        for i in self.settings.keys():
            path = self.settings[i]
            if os.path.isfile(path):
                self.mappings[i.lstrip(PATH_SYMBOL)] = path
            elif os.path.isdir(path):
                self.patterns[i.lstrip(PATH_SYMBOL)] = path
            else:
                error_message = "'" + path + "' is not a valid file or directory."
                raise aws_exceptions.ConfigError(error_message)

    def map_path(self, path):
        """
        Return the local path of the url given or raise an error if the page
        does not exist.
        """
        if path in self.mappings.keys():
            return self.mappings[path]
        for p in self.patterns.keys():
            if path.startswith(p):
                actual_path = path.replace(p, self.patterns[p], 1)
                if os.path.isfile(actual_path):
                    self.mappings[path] = actual_path
                    return self.mappings[path]
        raise aws_exceptions.HTTPErrorCode(404)
