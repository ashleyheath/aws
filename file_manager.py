import aws_exceptions
import config_manager

class FileManager():
    """Manages the files that the server serves up."""
    
    def __init__(self):
        """Initialise the file manager."""
        self.files = {}
    
    def load_file(self, path):
        """
        Load a file from memory or disk and return its contents or raise an
        exception if the file cannot be opened.
        """
        if path in self.files:
            return self.files[path]
        try:
            with open(path, 'r') as f:
                self.files[path] = f.read()
            return self.files[path]
        except IOError:
            raise aws_exceptions.HTTPErrorCode(500)
