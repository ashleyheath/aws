import aws_exceptions

COMMENT_CHARACTER = '#'
SEPERATOR_CHARACTER = ':'

class ConfigManager():
    """Manages instances of the Configurable class."""
    
    def __init__(self):
        self.configurables = []
    
    def add_configurable(configurable_obj):
        """Add a new configurable object to the manager."""
        self.configurables.append(configurable_obj)
  
    def update_configurables():
        """Update all the configurables that have been added to the manager."""
        for obj in self.configurables:
            obj.load_config(obj.config_name)
            obj.set_config()


class Configurable():
    """
    A class designed to be inherited by classes which have user-customisable
    settings.
    """
    
    def __init__(self, config_path):
        self.settings = {}
        self.config_path = config_path
     
    def load_config(self):
        """
        Load the settings from a file and store them in the object's
        settings dictionary.
        """
        try:
            with open(self.config_path, 'r') as f:
                lines = f.readlines()
            for l in lines:
                l = l.strip()
                if l.startswith(COMMENT_CHARACTER):
                    pass
                elif l == '':
                    pass
                else:
                    seperator = l.find(SEPERATOR_CHARACTER)
                    key = l[:seperator]
                    value = l[seperator+1:]
                    self.settings[key] = value
        except IOError:
            error_message = "Could not load '" + self.config_path + "'"
            raise aws_exceptions.ConfigError(error_message)
            
    def set_config(self):
        """
        A method to be overridden by child classes to allow them to implement
        their own behaviour when processing the settings that have been loaded.
        """
        pass
