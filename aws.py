#!/usr/bin/env python

"""The main script to run the server."""

import multiprocessing
import sys
import time

import aws_exceptions
import connection_listener
import connection_handler

__AWS_VERSION__ = 0.1

PACKAGES = ['http']

if __name__ == '__main__':
    sys.path.extend(PACKAGES)
    queue = multiprocessing.Queue()
    try:
        listener = connection_listener.ConnectionListener(queue)
    except aws_exceptions.BindError:
        print("Could not bind to specified port.")
        sys.exit()
    handler = connection_handler.ConnectionHandler(queue)
    handler.start()
    listener.start()
        
    def cleanup():
        """Cleanup handler and listener objects and then stop the program."""
        listener.stop()
        handler.stop()
        sys.exit()
    try:
        while True:
            time.sleep(60)
    except KeyboardInterrupt:
        cleanup()
