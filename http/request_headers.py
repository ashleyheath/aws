"""
Functions to process headers in the HTTP request which caused the given
response.
"""

import aws_decorators

import zlib

def Accept_Encoding_function(request, response):
    """Handle a request's Accept Encoding header."""
    encodings = request.headers['Accept-Encoding'].split(',')
    for encoding in encodings:
        encoding = encoding.strip()
        if (request.local_path, encoding) in Accept_Encoding_function.cache:
            response.body = Accept_Encoding_function.cache[(request.local_path, encoding)]
            return
    if 'deflate' in encodings:
        response.body = response.body.encode('zlib')
        Accept_Encoding_function.cache[(request.local_path, 'deflate')] = response.body
        response.headers['Content-Encoding'] = 'deflate'
Accept_Encoding_function.cache = {}

functions = {
  #'Accept'
  #'Accept-Charset'
  'Accept-Encoding':Accept_Encoding_function
  #'Accept-Language'
  #'Accept-Datetime'
  #'Authorization'
  #'Cache-Control'
  #'Connection'
  #'Cookie'
  #'Content-Length'
  #'Content-MD5'
  #'Content-Type'
  #'Date'
  #'Expect'
  #'From'
  #'Host'
  #'If-Match'
  #'If-Modified-Since'
  #'If-None-Match'
  #'If-Range'
  #'If-Unmodified-Since'
  #'Max-Forwards'
  #'Pragma'
  #'Proxy-Authorization'
  #'Range'
  #'Referer[sic]'
  #'TE'
  #'Upgrade'
  #'User-Agent'
  #'Via'
  #'Warning'
}
