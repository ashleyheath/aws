"""Functions to generate the values for HTTP responses."""

import datetime

import aws

# Example format: 'Tue, 15 Nov 1994 08:12:31 GMT'
DATE_FORMAT = '%a, %d %b %Y %H:%M:%S %Z'
SERVER_NAME = 'aws' + '/' + str(aws.__AWS_VERSION__)


CONNECTION = 'connection'
CLOSE = 'close'
KEEP_ALIVE = 'keep-alive'
def Connection_function(request, response):
    """Generate a value for the Connection response header."""
    if CONNECTION in request.headers:
        if request.headers[CONNECTION] == CLOSE:
            return CLOSE
    # Returns keep-alive by default as HTTP 1.1 considers all connections
    # to be persistent unless otherwise specified.
    return KEEP_ALIVE

def Content_Encoding_function(request, response):
    
    return

def Content_Length_function(request, response):
    """Generate a value for the Content Length response header."""
    return str(len(response.body))

import internet_media_types
def Content_Type_function(request, response):
    """
    Generate a value for the Content Type response header.
    Return None if the content type is unrecognised.
    """
    file_extension = request.local_path[request.local_path.rfind('.')+1:]
    if file_extension in internet_media_types.types:
        return internet_media_types.types[file_extension]
    else:
        return None

def Date_function(request, response):
    """Generate a value for the Date response header."""
    current_time = datetime.datetime.now()
    return current_time.strftime(DATE_FORMAT)

def Server_function(request, response):
    """Generate the value for the Server response header."""
    return SERVER_NAME

functions = {
    #'Accept-Ranges'
    #'Age'
    #'Allow'
    #'Cache-Control'
    'Connection':Connection_function,
    'Content-Encoding':Content_Encoding_function,
    #'Content-Language'
    'Content-Length':Content_Length_function,
    #'Content-Location'
    #'Content-MD5'
    #'Content-Disposition[19][20][21]'
    #'Content-Range'
    'Content-Type':Content_Type_function,
    'Date':Date_function,
    #'ETag'
    #'Expires'
    #'Last-Modified':Last_Modified_function
    #'Link'
    #'Location'
    #'P3P'
    #'Pragma'
    #'Proxy-Authenticate'
    #'Refresh'
    #'Retry-After'
    'Server': Server_function
    #'Set-Cookie
    #'Strict-Transport-Security'
    #'Trailer'
    #'Transfer-Encoding'
    #'Vary'
    #'Via'
    #'Warning'
    #'WWW-Authenticate'
}
