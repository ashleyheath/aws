"""Functions to handle the different types of HTTP request."""

import aws_exceptions
import http.message
import http.request_headers
import http.response_codes

GET     = 'GET'
HEAD    = 'HEAD'
POST    = 'POST'
PUT     = 'PUT'
DELETE  = 'DELETE'
TRACE   = 'TRACE'
OPTIONS = 'OPTIONS'
CONNECT = 'CONNECT'
PATCH   = 'PATCH'

method_names = frozenset([GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS,
                          CONNECT, PATCH])

def get_method_function(method):
    return methods[method]

def GET_method(file_manager, request):
    """Generate a response to a GET request."""
    response = http.message.Response(200, http.response_codes.codes[200], request)
    response.body = file_manager.load_file(request.local_path)
    response.process_headers()
    return response

def HEAD_method(file_manager, request):
    """Generate a response to a HEAD request."""
    response = GET_method(file_manager, request)
    response.body = ''
    return response

def POST_method(file_manager, request):
    """Generate a response to a POST request."""
    # Returns 501 Not Implemented
    response = aws_exceptions.HTTPErrorCode(501)
    return response
    
def PUT_method(file_manager, request):
    """Generate a response to a PUT request."""
    # Returns 501 Not Implemented
    response = aws_exceptions.HTTPErrorCode(501)
    return response
  
def DELETE_method(file_manager, request):
    """Generate a response to a DELETE request."""
    # Returns 501 Not Implemented
    response = aws_exceptions.HTTPErrorCode(501)
    return response
    
def TRACE_method(file_manager, request):
    """Generate a response to a TRACE request."""
    # Returns 501 Not Implemented
    response = aws_exceptions.HTTPErrorCode(501)
    return response
    
def OPTIONS_method(file_manager, request):
    """Generate a response to a OPTIONS request."""
    # Returns 501 Not Implemented
    response = AWSExceptions.HTTPErrorCode(501)
    return response
  
def CONNECT_method(file_manager, request):
    """Generate a response to a CONNECT request."""
    # Returns 501 Not Implemented
    response = AWSExceptions.HTTPErrorCode(501)
    return response
  
def PATCH_method(file_manager, request):
    """Generate a response to a PATCH request."""
    # Returns 501 Not Implemented
    response = AWSExceptions.HTTPErrorCode(501)
    return response

methods = {GET:GET_method, HEAD:HEAD_method, POST:POST_method, PUT:PUT_method,
           DELETE:DELETE_method, TRACE:TRACE_method, OPTIONS:OPTIONS_method,
           CONNECT:CONNECT_method, PATCH:PATCH_method}
