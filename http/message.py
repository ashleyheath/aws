"""
Classes and functions for the processing of HTTP messages and the creation of
objects to represent them.
"""

import time
import urllib

import aws_exceptions
import http.request_methods
import http.response_headers

HTTP_1_0 = 'HTTP/1.0'
HTTP_1_1 = 'HTTP/1.1'


class Message():
    """Base class for any HTTP message sent or recieved."""
    
    def __init__(self):
        """Initialise the message."""
        self.HTTP_version = ''
        self.headers = {}
        self.body = ''
        
        self.connection = None
    
    def build(self):
        """Return a string that is the HTTP message in full."""
        message = self.HTTP_version + ' ' + str(self.status_code)
        message += ' ' + self.status_description + '\r\n'
        for h in self.headers:
            message += h + ': ' + self.headers[h] + '\r\n'
        if self.body:
            message += '\r\n' + self.body
        return message


class Request(Message):
    """A class to store an HTTP request."""
    
    def __init__(self):
        """Initialise the request."""
        Message.__init__(self)
        self.method = ''
        self.path = ''
        self.local_path = ''


class Response(Message):
    """A class to store an HTTP response."""
    
    def __init__(self, code, description, request, headers={}, body='', version=HTTP_1_1):
        """Initialise the response"""
        Message.__init__(self)
        self.status_code = code
        self.status_description = description
        self.request = request
        self.headers = headers
        self.body = body
        self.HTTP_version = version
        # Requests and their responses share the same connection.
        self.connection = request.connection
    
    def process_headers(self):
        """Process the request's headers and generate the response's headers."""
        for header in self.request.headers.viewkeys():
            if header in http.request_headers.functions:
                http.request_headers.functions[header](self.request, self)
        headers = {}
        for header in http.response_headers.functions.keys():
            value = http.response_headers.functions[header](self.request, self)
            if value:
                headers[header] = value
        self.headers.update(headers)

def parse_request(req):
    """Take a HTTP request as a string and return it as a request object."""
    
    request = Request()
    
    req = req.replace('\r', '')
    
    rl_end = req.find('\n')
    if rl_end == -1:
        return aws_exceptions.HTTPErrorCode(400)
    request_line = req[:rl_end]
    
    if request_line.count(' ') != 2:
        return aws_exceptions.HTTPErrorCode(400)
    request.method, request.path, request.HTTP_version = request_line.split(' ')
    request.path = urllib.url2pathname(request.path)
    if request.method not in http.request_methods.method_names:
        return aws_exceptions.HTTPErrorCode(400)
    
    headers_end = req.find('\n\n', rl_end+1)
    if headers_end != -1:
        raw_headers = req[rl_end+1:headers_end]
        try:
            header_lines = raw_headers.split('\n')
            for i in header_lines:
                key, value = i.split(': ', 1)
                request.headers[key] = value
         #If split point is not present
        except ValueError:
            return aws_exceptions.HTTPErrorCode(400)
    
    if headers_end+1 < len(req) and headers_end != -1:
        request.body = req[headers_end+1:]
    
    return request
