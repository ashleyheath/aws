import datetime
import multiprocessing
import socket
import select
import collections
# For Python 3
try:
    import Queue
except ImportError:
    import queue as Queue
import time

import aws_exceptions
import file_manager
import http.message
import http.request_methods
import path_mapper

# Length of time before a persistent connection is closed.
CONNECTION_TIMEOUT = 5.0

class ConnectionHandler(multiprocessing.Process):
    """A custom process to process requests to the server."""
    
    def __init__(self, queue):
        """Initialise the process."""
        multiprocessing.Process.__init__(self)
        self.connections_queue = queue
        self.file_manager = file_manager.FileManager()
        self.path_mapper = path_mapper.PathMapper()
        self.connections = collections.OrderedDict()
    
    def run(self):
        """Run the connection handler process."""
        cons = self.connections.keys()
        inputs, outputs, errors = select.select(cons, cons, cons, 0)
        try:
            while True:
                # Add a new connection to self.connections if one exists in
                # self.connections_queue.
                if not self.connections_queue.empty():
                    unbuilt = self.connections_queue.get_nowait()
                    connection = multiprocessing.reduction.rebuild_socket(unbuilt[0], unbuilt[1], unbuilt[2], unbuilt[3])
                    connect_time = datetime.datetime.now()
                    self.connections[connection] = connect_time
                
                # Respond to a connection in self.connections if there exists one that
                # is available for reading.
                if inputs and outputs:
                    connection = inputs[0]
                    def close_connection():
                        connection.close()
                        del self.connections[connection]
                        del inputs[0]
                    try:
                        data = connection.recv(1024)
                        if data:
                            response = None
                            try:
                                request = http.message.parse_request(data)
                                request.local_path = self.path_mapper.map_path(request.path)
                                request.connection = connection
                                if request.method in http.request_methods.method_names:
                                    response = http.request_methods.get_method_function(request.method)(self.file_manager, request)
                                else:
                                    response = aws_exceptions.HTTPErrorCode(400)
                            except aws_exceptions.HTTPErrorCode as HTTPError:
                                response =  HTTPError
                            connection.sendall(response.build())
                            if 'Connection' in request.headers and request.headers['Connection'] == 'close':
                                close_connection()
                            else:
                                now = datetime.datetime.now()
                                start = self.connections[connection]
                                time_passed = (now - start).total_seconds()
                                if time_passed > CONNECTION_TIMEOUT:
                                    close_connection()
                    except socket.error as error:
                        del inputs[0]

                else:
                    cons = self.connections.keys()
                    inputs, outputs, errors = select.select(cons, cons, cons, 0)
        except Queue.Empty:
            pass
        except KeyboardInterrupt:
            self.stop()
    
    def stop(self):
        """Cleanup the process so the program can exit cleanly."""
        self.connections_queue.close()
        for c in self.connections:
            c.shutdown(socket.SHUT_RDWR)
            c.close()
