"""Custom exceptions for the server."""

import http.message
import http.response_codes

class FatalError(Exception):
    """
    Raised when encountering an error that prevents further execution of the
    program.
    """
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr("Fatal Error: " + self.value)

class BindError(FatalError):
    """Raised when a socket can not be bound to a port."""

class ConfigError(FatalError):
    """Raised when encountering an error processing a config file."""
    def __init__(self):
        FatalError.__init__(self)
    def __str__(self):
        return repr("Config Error: " + self.value)

class HTTPErrorCode(Exception, http.message.Response):
    """
    Raised or returned when an error is encountered that has a designated
    error code in the http specification.
        The exception doubles as an HTTP response so that it can be built and
    sent to the client if appropriate.
    """
    def __init__(self, code):
        http.message.Response.__init__(self, code,
                                          http.response_codes.codes[code])
